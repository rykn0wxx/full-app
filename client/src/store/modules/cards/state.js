// https://vuex.vuejs.org/en/state.html

export default {
  cards: [
    {
      card_id: 1,
      card_content: 'card one of list one of board one',
      list_id: 1,
      order: 1
    }, {
      card_id: 2,
      card_content: 'card two of list one of board one',
      list_id: 1,
      order: 2
    }, {
      card_id: 3,
      card_content: 'card one of list two of board one',
      list_id: 2,
      order: 1
    }
  ]
}
