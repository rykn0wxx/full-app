// https://vuex.vuejs.org/en/state.html

export default {
  boards: [
    {
      board_id: 1,
      board_name: 'One',
      owner_id: 1,
      group_id: 1
    }, {
      board_id: 2,
      board_name: 'Two',
      owner_id: 1,
      group_id: 1
    }
  ]
}
