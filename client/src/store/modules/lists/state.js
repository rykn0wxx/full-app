// https://vuex.vuejs.org/en/state.html

export default {
  lists: [
    {
      list_id: 1,
      list_name: 'List One of board one',
      board_id: 1,
      order: 1
    }, {
      list_id: 2,
      list_name: 'List Two of board one',
      board_id: 1,
      order: 2
    }, {
      list_id: 3,
      list_name: 'List One of board two',
      board_id: 2,
      order: 1
    }
  ]
}
