# client

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Reference Links
https://wrappixel.com/demos/admin-templates/material-pro/dark/index.html

https://www.ampleadmin.wrappixel.com/ampleadmin-html/ampleadmin-dark/index.html

http://eliteadmin.themedesigner.in/demos/bt4/dark/ui-cards.html

https://colorlib.com/polygon/gentelella/index3.html
