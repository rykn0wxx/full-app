Rails.application.routes.draw do
  resources :cards
  resources :lists
  resources :boards
  resources :members
  resources :groups
  resources :users
  # jsonapi_resources :users

  post 'user_token' => 'user_token#create'
  root :to => 'home#index'
end
