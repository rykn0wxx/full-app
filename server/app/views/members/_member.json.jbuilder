json.extract! member, :id, :user_id, :group_id, :is_admin, :created_at, :updated_at
json.url member_url(member, format: :json)
